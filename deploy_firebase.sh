#!/usr/bin/env bash

npm install -g firebase-tools

firebase appdistribution:distribute app/build/outputs/apk/release/*.apk   \
    --token "$FIREBASE_CI_TOKEN" \
    --app "$FIREBASE_APP_ID" \
    --release-notes release-notes.txt --testers-file testers.txt
