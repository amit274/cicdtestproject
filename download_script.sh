#!/usr/bin/env bash

MANIFEST_URL="https://github.com/amit517/garbage/blob/main/manifest.json"
TFT_MODEL_URL="https://github.com/amit517/garbage/raw/main/model.tflite"
MANIFEST_FILENAME="manifest.json"
TFT_MODEL_FILENAME="model.tflite"

cd app/src/main/assets/automl

echo "Downloading manifest.json and model.tflite ---->>>"

if curl --silent -o "${MANIFEST_FILENAME}" -L "${MANIFEST_URL}" && curl --silent -o "${TFT_MODEL_FILENAME}" -L "${TFT_MODEL_URL}"; then
    echo "files in automl after complete download ---->>>"
        pwd
          ls -la
          cd ..
          cd ..
          cd ..
          cd ..
          cd ..
          pwd
        fi

echo "Providing JKS and key.properties ---->>>"

echo ${KEYSTORE_FILE} | base64 -d >tdd_store_jks.jks
echo ${KEY_STORE_PROP} | base64 -d > key.properties

# todo unzip method not working. will check later

#ZIP_URL="https://github.com/amit517/garbage/raw/main/automl.rar"
#ZIP_FILE_NAME="automl.zip"

#if curl --silent -o "${ZIP_FILE_NAME}" -L "${ZIP_URL}"; then
#    echo "Before unzip"
#    ls -la
#    unzip "${ZIP_FILE_NAME}"
#        if [[ -f "${ZIP_FILE_NAME}" ]]; then
#            echo "Removing the file.."
#            rm -f "${ZIP_FILE_NAME}"
#            fi
#    echo "files in automl after complete download"
#      ls -la
#      cd ..
#      cd ..
#      cd ..
#      cd ..
#      cd ..
#      pwd
#    fi
#echo "Done!"
